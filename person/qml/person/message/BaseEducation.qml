import QtQuick 2.1
import QtQuick.Controls 1.1
Rectangle {
    id:root
    anchors.fill: parent
    anchors.margins: 5
        Rectangle{
            id:line
            x:2
            width: parent.width
            height:1
            color: "#c0acac"
            anchors.top: parent.top
            anchors.margins: 2
            border.color: "#e8e1e1"
            opacity: .4
        }
        Rectangle{
            id:edu1
            x:2
            width: parent.width-4
            height:30
            color: "#c0acac"
            anchors.top: line.bottom
            anchors.margins: 2
            border.color: "#e8e1e1"
            radius: 5
            border.width: 1
            Text{
                anchors.centerIn: parent
                font.bold: true
                font.pixelSize: 18
                text:qsTr("collegeTime")
            }
        }
        Rectangle{
            id:edupart1
            x:2
            width: parent.width
            height:(parent.height - line.height -70)/2
            anchors.top: edu1.bottom
            anchors.topMargin:2
            border.color: "#e8e1e1"
            color: "red"
            opacity: .2
            radius: 5
            border.width: 1
        }
        Row{
            id:row1
            spacing: 5
            anchors.fill: edupart1
            anchors.centerIn: edupart1
            Item{
                id:labArea1
                width:90
                height:parent.height-20
                Column{
                    spacing: 5
                    anchors.fill: parent
                    anchors.topMargin: 5
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("gradYer")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("gradColl")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("specia")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("eduType")
                        }
                    }
                }
            }
            Item{
                x:3
                width:parent.width-100
                height:labArea1.height
                Column{
                    spacing: 5
                    anchors.fill: parent
                    anchors.topMargin: 5
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.gradYearZh:config.gradYearEn
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.zhCollege : config.enCollege
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.zhMajor : config.enMajor
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("fullTime")
                        }
                    }
                }
            }
        }
        Rectangle{
            id:edu2
            x:2
            width: parent.width-4
            height:30
            color: "#c0acac"
            anchors.top: edupart1.bottom
            anchors.margins: 2
            border.color: "#e8e1e1"
            radius: 5
            border.width: 1
            Text{
                anchors.centerIn: parent
                font.bold: true
                font.pixelSize: 18
                text:qsTr("undergrad")
            }
        }
        Rectangle{
            id:edupart2
            x:2
            width: parent.width
            height:edupart1.height
            anchors.top: edu2.bottom
            anchors.topMargin:2
            border.color: "#e8e1e1"
            color: "red"
            opacity: .2
            radius: 5
            border.width: 1
        }
        Row{
            id:row2
            spacing: 5
            anchors.fill: edupart2
            anchors.centerIn: edupart2
            Item{
                id:eduMsg2
                width:90
                height:parent.height-20
                Column{
                    spacing: 5
                    anchors.fill: parent
                    anchors.topMargin: 5
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("applyYer")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("school")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("specia")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:"学习方式"
                        }
                    }
                }
            }
            Item{
                x:3
                width:parent.width-100
                height:parent.height-20
                Column{
                    spacing: 5
                    anchors.fill: parent
                    anchors.topMargin: 5
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:"2013年"
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:"北京大学"
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:"计算机科学与应用"
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:parent.height/4
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:"自学考试（已考4科）"
                        }
                    }
                }
            }
        }
}
