import QtQuick 2.0
ListModel {
    id: mainModel
    ListElement { titleMsg: "个人信息";name:"personMsg";content:"姓名,联系方式,学历等等个人基本资料..."}
    ListElement { titleMsg: "教育背景";name:"education";content:"个人的教育背景..."}
    ListElement { titleMsg: "专业技能";name:"skill";content:"个人的专业技能相关..."}
    ListElement { titleMsg: "项目经验";name:"projects";content:"所做过的项目以及相关的工作经验..."}
    ListElement { titleMsg: "自我认识";name:"assessment";content:"自我认识以及自我评价..."}
}
