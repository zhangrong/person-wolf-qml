import QtQuick 2.0
import QtQuick.Controls 1.1
import "qrc:/tool/component" as Tools
Item {
    id: mainView
    width: parent.width
    height: parent.height
    ListView {
        id: list
        anchors.top: topBar.bottom
        anchors.bottom: footBar.top
        width: parent.width
        height:parent.height-150
        model: MessageModel{}
        delegate:MessageDelegate{}
    }
    Tools.ToolBar {
        id: topBar
        width: parent.width
        height: 90
        opacity: 1
        backButtonShow: true
        title: load.pageTitle
        onBack: {
            load.setActiveItemByName("center");
        }
    }
    Tools.ToolBar {
        id: footBar
        width: parent.width
        backButtonShow: false
        anchors.bottom: parent.bottom
        height: 60
        opacity: 1
        Image {
                source:"../images/wolf_head.png"//背景图片
                anchors.left: parent.left
                anchors.leftMargin: 15
                height: parent.height  - 10
                width: parent.height  - 10
                anchors.verticalCenter: parent.verticalCenter
         }
    }
    Text{
        id:footMsg
        anchors.verticalCenter: footBar.verticalCenter
        anchors.right: parent.right
        text:qsTr("promise")
        font.bold: true
        font.pointSize: 16
        color: "black"
    }
}
