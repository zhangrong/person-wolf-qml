import QtQuick 2.1
import QtQuick.Controls 1.1
Item {
    id:root
    anchors.fill: parent
    anchors.margins: 5
    Rectangle{
        border.color: "#cccccc"
        border.width: 1
        anchors.margins:3
        opacity: 10
        anchors.fill: parent
        x:8
        Text{
            id:gr1
            anchors.top: parent.top
            anchors.topMargin:3
            height: 20
            x:20
            text:qsTr("baseMsg")
        }
        Row{
            id:row
            spacing: 5
            x:5
            width:parent.width-10
            height:parent.height-36
            anchors.top:gr1.bottom
            anchors.topMargin: 10
            Item{
                id:labArea
                width:90
                height:(parent.height-36)*.4
                Column{
                    spacing: 10
                    anchors.fill: parent
                    anchors.topMargin: 10
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("name")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("sex")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("education")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        x:3
                        radius: 5
                        border.color: "#cccccc"
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("phone")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("email")
                        }
                    }
                }
            }
            Item{
                x:3
                width:(parent.width-100)*.4
                height:(parent.height-36)*.4
                Column{
                    spacing: 10
                    anchors.fill: parent
                    anchors.topMargin: 10
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.zhName:config.enName
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.zhSex:config.enSex
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.zhEdu:config.enEdu
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:config.phone
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-50)/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:config.email
                        }
                    }
                }
            }
            Item{
                x:3
                width:(parent.width-100)*.6
                height:(parent.height-36)*.4
                Rectangle{
                    anchors.fill: parent
                    anchors.margins: 10
                    anchors.centerIn: parent
                    radius: 10
                    border.color: "#cccccc"
                    border.width: 1
                    Image{
                        source: "../images/wolf_head.png"
                        anchors.centerIn: parent
                        anchors.margins: 10
                    }
                }
            }
        }
        Rectangle{
            id:baseArea
            x:2
            width:parent.width-4
            height:labArea.height
            anchors.top:gr1.bottom
            anchors.topMargin: 10
            border.color: "#e8e1e1"
            border.width: 1
            color: "#e7fdf4"
            radius: 5
            opacity: .2
        }
        Rectangle{
            id:line
            x:2
            width: baseArea.width
            height:1
            color: "#c0acac"
            anchors.top: baseArea.bottom
            anchors.margins: 2
            border.color: "#e8e1e1"
            opacity: .4
        }
        Rectangle{
            id:moreMsgArea
            x:2
            width: baseArea.width
            anchors.top: line.bottom
            anchors.bottom: parent.bottom
            anchors.topMargin: 10
            border.color: "#e8e1e1"
            color: "#e7fdf4"
            opacity: .2
            radius: 5
            border.width: 1
        }
        Row{
            id:row1
            spacing: 10
            anchors.fill: moreMsgArea
            Item{
                id:labArea1
                width:90
                height:parent.height
                Column{
                    spacing: 5
                    anchors.fill: parent
                    anchors.topMargin: 10
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:grad.height+10
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            id:grad
                            anchors.centerIn: parent
                            text:qsTr("graduate")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.color: "#cccccc"
                        border.width: 1
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:grad.height+10
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("academy")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:grad.height+10
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("codeAge")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        x:3
                        radius: 5
                        border.color: "#cccccc"
                        width: parent.width-6
                        height:(parent.height-3*(grad.height + 30))/2
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("domain")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-3*(grad.height + 30))/2
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:qsTr("others")
                        }
                    }
                }
            }
            Item{
                x:3
                width:parent.width-100
                height:parent.height
                Column{
                    spacing: 5
                    anchors.fill: parent
                    anchors.topMargin: 10
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:edu.height + 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            id:edu
                            anchors.centerIn: parent
                            text:config.eduYear
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:edu.height + 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:currentLanguage == "zh-CN"?config.zhCollege : config.enCollege
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:edu.height + 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.centerIn: parent
                            text:config.year +" "+ qsTr("year")
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-3*(edu.height + 30))/2
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.verticalCenter: parent.verticalCenter
                            width:parent.width-20
                            height: parent.height - 10
                            x:10
                            wrapMode: Text.WordWrap
                            elide: Text.ElideRight//超过多余的地方用...代替
                            text:currentLanguage == "zh-CN"?config.zhDomain : config.enDomain
                        }
                    }
                    Rectangle{
                        color: "#e8e1e1"
                        border.width: 1
                        border.color: "#cccccc"
                        x:3
                        radius: 5
                        width: parent.width-6
                        height:(parent.height-3*(edu.height + 30))/2
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text{
                            anchors.verticalCenter: parent.verticalCenter
                            height: parent.height - 10
                            x:10
                            wrapMode: Text.WordWrap
                            width:parent.width-10
                            elide: Text.ElideRight//超过多余的地方用...代替
                            text:currentLanguage == "zh-CN"?config.zhOther : config.enOther
                        }
                    }
                }
            }
        }
    }
}
