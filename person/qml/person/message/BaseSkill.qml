import QtQuick 2.1
import QtQuick.Controls 1.1
Rectangle {
    id: root
    anchors.fill: parent
    Rectangle{
        id:bg
        x:5
        y:5
        width: parent.width-10
        height:parent.height-10
        anchors.centerIn: parent
        color: parent.color
        border.color: "#ccc"
        border.width: 1
        radius: 5
    }
    Rectangle{
        id:topMsg
        width: bg.width - 20
        height: msg.height + 10
        anchors.top: bg.top
        anchors.topMargin: 5
        anchors.horizontalCenter: bg.horizontalCenter
        z:listView.z +1
        Text {
            id: msg
            anchors.centerIn: parent
            x:5
            clip:true//是否允许被修剪
            color:"red" //颜色
            style: Text.Raised
            font.pixelSize: 17
            styleColor: "red"
            width: parent.width-10
            wrapMode:Text.WrapAnywhere
            text: qsTr("skill")
        }
    }
    Rectangle {
        id:listView
        width:topMsg.width
        height: bg.height - topMsg.height - 15
        anchors.top: topMsg.bottom
        anchors.topMargin: 2
        border.color: "#ccc"
        border.width: 1
        x:15
        ListView{
            id:list
            anchors.fill: listView
            clip:true//可以让多余的内容在列表内不显示
                Component {
                    id: contactsDelegate
                    Item {
                        id: wrapper
                        width: list.width
                        height: contactInfo.height
                        Column{
                            id:contactInfo
                            spacing: 2
                            x:5
                            Row{
                                spacing: 10
                                width:listView.width - 10
                                anchors.horizontalCenter: parent.horizontalCenter
                                height:nameArea.height
                                Text {
                                    id: nameArea
                                    width:parent.width/2
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.leftMargin: 20
                                    text: qsTr("技能 : " + name)
                                }
                                Text {
                                   width: parent.width/2
                                   anchors.rightMargin: 20
                                   anchors.verticalCenter: parent.verticalCenter
                                   text: qsTr("使用周期 : "+ time)
                                }
                            }
                            Text{
                                id:skillArea
                                width:listView.width - 10
                                anchors.horizontalCenter: parent.horizontalCenter
                                height:nameArea.heigh
                                text: qsTr("类别: "+skill+"/"+category+"/"+belong)
                            }
                            Text{
                                id:contentArea
                                width:listView.width - 10
                                anchors.horizontalCenter: parent.horizontalCenter
                                height:nameArea.heigh
                                wrapMode: Text.WordWrap
                                text: qsTr("简介 : " + content)
                            }
                            Text{
                                id:defectArea
                                width:listView.width - 10
                                anchors.horizontalCenter: parent.horizontalCenter
                                height:nameArea.heigh
                                wrapMode: Text.WordWrap
                                text: qsTr("缺陷与不足 : " + defect)
                            }
                            Text{
                                id:projectsArea
                                width:listView.width - 10
                                anchors.horizontalCenter: parent.horizontalCenter
                                height:nameArea.heigh
                                wrapMode: Text.WordWrap
                                text: qsTr("相关项目 : " + projects)
                            }
                            Rectangle{
                                color:"#cccccc"
                                height:5
                                width: listView.width - 10
                            }
                        }
                    }
                }
                model: skillModel
                delegate: contactsDelegate
                focus: true
                section.property: "skill"
                section.criteria: ViewSection.FullString
                section.delegate: Rectangle {
                        width: listView.width
                        height: 30
                        color: "lightsteelblue"
                        Text {
                            text: section
                            font.bold: true
                            font.pixelSize: 20
                        }
                   }
                Component.onCompleted: list.section.labelPositioning |= ViewSection.CurrentLabelAtStart
            }
        ListModel {
            id:skillModel
            ListElement {
                name: "sencha touch"
                category:'javascript'
                skill:"html5"
                belong:"框架"
                time:"1年半"
                content:'使用时间最长,从用命令创建项目,到项目搭建,到界面以及功能编写到组件的扩展再到项目打包一套流程全通'
                defect:"android平台3.0版本前在不同机器上兼容性差,维护难度偏大,这也是本人觉得比较头疼的问题"
                projects:"电子菜谱【在线版和离线版】  点菜机【在线版和离线版】 点菜系统管理器   微信点菜 "
            }
            ListElement {
                name: "phonegap/cordova"
                category:'javascript'
                skill:"html5"
                time:"1年半"
                belong:"框架"
                content:"插件的扩展，比如百度定位,讯飞语音插件,扫描二维码插件,数据加密插件,以及常用的获取坐标,文件下载或上传,拍照功能等等"
                defect:"消耗内存大,ios平台没有去研究过,相对偏弱"
                 projects:"同上"
            }
            ListElement {
                name: "css/css3"
                category:'css/css3'
                skill:"html5"
                time:"1年半"
                belong:"语言"
                content:"熟练使用其对页面元素布局等等,定位,浮动,边距等等细节的调整,以及css3的动态效果的使用"
                defect:"css3效果在android平台3.0版本前尽可能的少用"
                projects:"同上"
            }
            ListElement {
                name: "json"
                time:"2年"
                category: "javascript"
                skill:" js"
                content:"基本上在应用程序开发中用到的数据格式都是json格式,对其key-value模式的操作方式比较得心应手"
                defect:"层次太多会在使用的过程中会造成失误"
                belong:"库类"
                projects:"同上"
            }
            ListElement {
                name: "Qt Quick/qml"
                time:"2个月"
                category: "Qt"
                skill:"c++ & js"
                belong:"框架"
                content:"了解的时间短,因其语法像极了sencha touch (json模式),所以入手挺快,从页面布局,到界面,到功能,一通百通"
                defect:"性能较好,apk生成大,尚未花时间去研究"
                projects:"Qt版单机版点菜机,Qt 版微信点菜(未带微信扫一扫功能),以及本简历"
            }
            ListElement {
                 name: "cocos2dx"
                category: "cocos2dx"
                skill:"c++"
                belong:"框架/引擎"
                time:"工作之余学习"
                content:"还在学习当中,对其语法及项目结构有初步认识"
                defect:""
                projects:""
            }
            ListElement {
                name: "jquery"
                category:'javascript'
                skill:"js"
                time:"3个月"
                content:"当初做jquery moble 的时候有所了解,会使用其对网页元素进行操作,以及修改对应的jquery插件为己用"
                defect:""
                projects:""
                 belong:"库类"
            }
            ListElement {
                name: "ExtJs"
                category:'javascript'
                skill:"js"
                time:"1年半"
                content:"sencha touch 就是此库类的框架,仅仅限于使用其做开发"
                defect:""
                projects:""
                belong:"库类"
            }
        }
    }
}
