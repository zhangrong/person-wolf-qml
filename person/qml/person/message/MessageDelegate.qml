import QtQuick 2.1
import QtQuick.Controls 1.1
import "qrc:/tool/component" as Tools
Rectangle {
    id: recipe
    //定义一个属性，目的是为了在详细页淡出的时候透明化详细页
    property real detailsOpacity : 0
    width: parent.width
    height: 90
    color: "#d8dadd"
    opacity:0.7
    border.color: "#cccccc"
    border.width: 1
    Rectangle {
        height: 1
        color: "white"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }
    Rectangle {
        height: 1
        color: "white"
        anchors.top: parent.top
        anchors.topMargin: 1
        anchors.left: parent.left
        anchors.right: parent.right
    }
    gradient: Gradient {
        GradientStop { position: 0 ; color: "white" }
        GradientStop { position: 1 ; color: "#ccc" }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            recipe.state = 'Details';
            showDetailPage();
        }
        anchors.margins: -5
    }
    Tools.ToolBar{
        id:textTopBar
        anchors.top: parent.top
        height:90
        width: parent.width
        title:qsTr(titleMsg)
        visible: false
        z:parent.z + 1
        backButtonShow: true
        onBack: {
            recipe.state = '';
        }
    }
    Text {
        id:titleArea
        text: titleMsg
        font.bold: true
        font.pointSize: 18
        width: parent.width - 30
        x:15
        anchors.top: parent.top
        anchors.topMargin: 20
        color: "black"
    }
    Text {
        id:contentArea
        text: content
        font.bold: true
        font.pointSize: 15
        x:15
        width: parent.width - 75
        elide: Text.ElideRight
        anchors.top: titleArea.bottom
        anchors.topMargin: 8
        color: "black"
    }
    Image{
        id:nextImg
        source: "../images/next.png"
        width:50
        height:50
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 10
    }
    StackView {
        id: detailView
        anchors.top:textTopBar.bottom
        visible: false
        anchors.bottom: parent.bottom
        width: parent.width
        height: parent.height-90
    }
    states: State {
        name: "Details"
        //属性改变
        PropertyChanges { target: topBar; visible:false}
        PropertyChanges { target: footBar; visible:false}
        PropertyChanges { target: titleArea; visible:false }
        PropertyChanges { target: contentArea; visible:false }
        PropertyChanges { target: nextImg; visible:false }
        PropertyChanges { target: detailView;visible:true}
        PropertyChanges { target: footMsg;visible:false}
        PropertyChanges { target: textTopBar;visible:true}
        PropertyChanges { target: recipe; opacity:10}
        PropertyChanges { target: recipe; detailsOpacity: 1; x: 0 } // 详细显示
        PropertyChanges { target: recipe; height: mainView.height } // 高等于屏幕的高
        // 移动顶部的列表，使y轴的值扩大到recipe的y
        PropertyChanges { target: recipe.ListView.view; explicit: true; contentY: recipe.y +90}
        // 不允许移动当前视图
        PropertyChanges { target: recipe.ListView.view; interactive: false }
    }
    transitions: Transition {
        // 确保状态变化平稳
        ParallelAnimation {
            ColorAnimation { property: "color"; duration: 150 }//颜色变化在0.5秒内完成
            NumberAnimation { duration: 150; properties: "detailsOpacity,x,contentY,height,width" }
        }
    }
    /**
      * 根据内容进入对应的界面
      */
    function showDetailPage(){
        switch(name){
        case "personMsg":
          detailView.push(Qt.resolvedUrl("BaseMessage.qml"));
          break;
        case "education":
            detailView.push(Qt.resolvedUrl("BaseEducation.qml"));
          break;
        case "skill":
            detailView.push(Qt.resolvedUrl("BaseSkill.qml"));
          break;
        case "projects":
            detailView.push(Qt.resolvedUrl("BaseProjects.qml"));
          break;
        case "assessment":
            detailView.push(Qt.resolvedUrl("BaseAssessment.qml"));
          break;
        }
    }
}
