import QtQuick 2.1
import QtQuick.Controls 1.1
import "../center"
Rectangle {
    id: root
    anchors.fill: parent
    Rectangle {
        id:listView
        width: parent.width
        height:parent.height-20
        border.color: "#ccc"
        border.width: 1
        ListView {
            id: view
            width:parent.width
            height:parent.height*.7
            model: ProjectModelForZh{}
            delegate:projectDelegate
            highlightRangeMode: ListView.StrictlyEnforceRange
            orientation: ListView.Horizontal
            snapMode: ListView.SnapOneItem;
            flickDeceleration: 2000
            cacheBuffer: 200
        }
        Component {
            id:projectDelegate
            Image {
                id:img
                width: view.width
                height: view.height
                anchors.margins: 10
                source: image
            }
        }
        Rectangle{
            id:baseMsg
            color: "#eae7e7"
            opacity:0.3
            width:parent.width
            height:parent.height*.3
            anchors.bottom:parent.bottom
            Text{
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.bottomMargin: 10
                anchors.topMargin: 10
                text:"点我查看更多>>"
                MouseArea{
                    anchors.fill: parent
                    onClicked: console.info("show more")
                }
            }
        }
        Text{
            id:title
            anchors.top:baseMsg.top
            anchors.bottom: baseMsg.bottom
            anchors.margins: 5
            font.pointSize: 12
            color: "#383737"
            text:"<font color='red' size='17'>简介：</font>"+content
            width:parent.width-20
            wrapMode: TextEdit.Wrap//自动换行
            elide: Text.ElideRight//超过多余的地方用...代替
        }
        Rectangle {
            id:indexBar
            width: root.width;
            anchors.top: listView.bottom
            height:20
            color: "red"
            opacity: 0.4
            Row {
                anchors.centerIn: parent
                spacing: 20
                Repeater {
                    model: view.model.count
                    Rectangle {
                        width: 5; height: 5
                        radius: 3
                        color: view.currentIndex == index ? "#a00909" : "white"
                        MouseArea {
                            width: 20; height: 20
                            anchors.centerIn: parent
                            onClicked: view.currentIndex = index
                        }
                    }
                }
            }
        }
    }
}
