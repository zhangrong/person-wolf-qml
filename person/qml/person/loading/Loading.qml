import QtQuick 2.0
import "../loading"
import "../center"
Item {
    id:load
    width: parent.width
    height: parent.height
    //记录姓名区域是否被点击,以便在计时时作出判断
    property bool txtTap: false
    //记录上一次的操作界面
    property string lastPage
    //页面标题
    property string pageTitle
    //判断语言框对话框是否显示
    property bool open: false
    //语言设置
    property var language
    //把界面加入到主窗体中[参数分别界面的名字,简介,对应代码文件的地址链接]
    function addView(name, desc, url)
    {
        cmpModel.append({"name":name, "description":desc, "url":url})
    }
    //根据index显示对应的页面
    function setActiveItemByIndex(index) {
        stackView.push(cmpModel.get(index).url)
    }
    //根据name显示对应的页面
    function setActiveItemByName(name) {
        for (var i = 0; i < cmpModel.count; i++) {
            var tempModel = cmpModel
            var obj = cmpModel.get(i)
            if (obj.name === name) {
                return stackView.push(obj.url)
            }
        }
    }
    //存放界面信息的model
    ListModel {
        id: cmpModel
    }
    Item {
        id:sky
        width:parent.width
        height:parent.height
        Splash {  }
        Center {
            id: menu
            opacity: 0
            timeRun:0
            anchors.fill: parent
            anchors.leftMargin: 2*sky.width
        }
        SequentialAnimation {
            running: true
            PauseAnimation { duration: 1300 }
            ParallelAnimation {
                NumberAnimation { target: menu; property: "opacity"; to: 1; duration: 600; easing.type: Easing.InOutQuad }
                NumberAnimation { target: menu; property: "anchors.leftMargin"; to: 0; duration:1000; easing.type: Easing.OutQuint }
                NumberAnimation { target: menu; property: "timeRun"; to: 1; duration:1000; easing.type: Easing.OutQuint }
              }
        }
    }
}
