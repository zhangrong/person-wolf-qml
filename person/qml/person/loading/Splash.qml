import QtQuick 2.0
import QtGraphicalEffects 1.0
Rectangle {
    id: root
    anchors.fill: parent
    color: "transparent"
    Component.onCompleted: {
        introTransation.start();
    }
    Text {
        id: title
        anchors.fill: parent
        visible: false
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 20
        color: "white"
        text: qsTr("hello")
    }
    Rectangle{
        id:msgArea
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        width: parent.width - 40
        x:20
        height:50
        border.width: 1
        opacity: .4
        radius: 5
        border.color: "white"
    }
    Text{
        id:msgText
        anchors.centerIn: msgArea
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 15
        color: "white"
        text: qsTr("user")
    }
    FastBlur {
        id: blur
        anchors.fill: parent
        source: title
        radius: 100
        transform: [
            Scale { id: scale; origin.x: parent.width / 2; origin.y: parent.height / 2; xScale: 0.2; yScale: 0.2 }
        ]
    }
    ParallelAnimation {
        id: introTransation
        NumberAnimation { target: blur; property: "radius"; to: 0; duration: 800; easing.type: Easing.InOutQuad }
        NumberAnimation { target: scale; property: "xScale"; to: 1; duration: 1000; easing.type: Easing.OutQuad }
        NumberAnimation { target: scale; property: "yScale"; to: 1; duration: 1000; easing.type: Easing.OutQuad }
        NumberAnimation { target: blur; property: "opacity"; from: 0; to: 1; duration: 500 }
        NumberAnimation { target: msgArea; property: "opacity"; to: 0; duration: 1000 }
        NumberAnimation { target: msgText;property: "opacity"; to: 0; duration: 2000 }
        SequentialAnimation {
            PauseAnimation { duration: 1200 }
            NumberAnimation { target: blur; property: "opacity"; to: 0; duration: 500 }
            ScriptAction {
                script: {
                   root.destroy();
                }
            }
        }
    }
}
