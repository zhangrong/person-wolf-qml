import QtQuick 2.0
import QtQuick.Controls 1.1
import Language 1.0
import "qrc:/tool/component" as Tools
import "loading"
Item {
    height:600//初始化大小
    width: 400
    StackView {//一个存放所有界面的栈
       id: stackView
       anchors.fill: parent
       initialItem:Loading{//初始化界面
           id: load
           language:lan
           anchors.fill: parent
           Component.onCompleted: {//进入Loadding界面加载的方法
               addView("main", "首页",  Qt.resolvedUrl("main/Main.qml"));
               addView("message", "基本信息",  Qt.resolvedUrl("message/Message.qml"));
               addView("content", "信息组件",  Qt.resolvedUrl("content/Content.qml"));
               addView("center", "个人中心",  Qt.resolvedUrl("center/Center.qml"));
           }
      }
    }
    Language{
        id:lan
    }
    //背景图片
    Image{
        source:"images/background.png"
        z:stackView.z -1
        anchors.fill: parent
    }
    Item {
        QtObject {
            id: config
            property string zhName:"张荣"
            property string enName:"Helem"
            property string zhSex:"男"
            property string enSex:"Male"
            property string zhEdu: "大专"
            property string enEdu: "College Diploma"
            property string phone: "13451647445"
            property string email: "helem_2013@qq.com"
            property string eduYear: "2010"
            property string year:"2"
            property string zhCollege: "长江职业学院（湖北武汉）"
            property string enCollege: "ChangJiang College (WuHan HuBei)"
            property string zhDomain: "跨平台的app开发[js(Sencha Touch,Phonegap,jQuery Mobile,Qt Quick),c++(Qt,Cocos2dx)]"
            property string enDomain: "Cross-platform app development by using JavaScript and some of C++ Code such as Sencha Touch,Phonegap,jQuery Mobile,Qt Quick,Qt,Cocos2dx"
            property string zhOther: "近10来个android平台的app开发和维护的经验,IOS平台一个,都是用Js框架（Sencha Touch 和 Qt Quick(qml)）开发的，当然java和C++部分多少涉及点"
            property string enOther: "Nearly 10 android apk and 1 IOS Ipa apps development and maintenance experience, , all of them used Js framework (Sencha Touch and Qt Quick (QML) development, of course,used some of Java and c++ code "
            property string gradYearZh: "2010 年毕业"
            property string gradYearEn: "Graduated in 2010"
            property string zhMajor: "Java 软件"
            property string enMajor: "Java Program"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
//            property string eduYear: "2010"
        }
    }
}

