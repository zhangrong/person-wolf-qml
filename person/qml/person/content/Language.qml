import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
Item {
    id: root
    width: parent.width
    height: parent.height
    Rectangle {
        id: bg
        x: 5
        y: 5
        width: parent.width - 10
        height: parent.height - 10
        anchors.centerIn: parent
        color: "#ccc"
        opacity: .5
        border.color: "#ccc"
        border.width: 1
        radius: 5
    }
    Column {
        anchors.centerIn: bg
        spacing: 20
        ExclusiveGroup {
            id: group
        }
        RadioButton {
            style:rd
            text: "简体中文"
            exclusiveGroup: group
            checked: currentLanguage == "zh-CN"?true:false
            onClicked:{
                if(checked){
                   changeLanguage("zh-CN");
                }
              }
        }
        RadioButton {
            style:rd
            text: "English"
            exclusiveGroup: group
            checked: currentLanguage == "en-US"?true:false
            onClicked:{
                if(checked){
                    changeLanguage("en-US");
                }
              }
        }
    }
    Component{
        id:rd
        RadioButtonStyle {
                spacing: 20
                indicator: Rectangle {
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 20
                        border.color: "#020302"
                        border.width: 5
                        Rectangle {
                            anchors.fill: parent
                            visible: control.checked
                            radius: 20
                            color: "#f77171"
                            border.color: "#180404"
                            anchors.margins: 4
                        }
                }
        }
    }
    /**
      * 更换语言
      */
    function changeLanguage(languageValue){
        if(languageValue != currentLanguage){
           // Com.value = languageValue;
            var component = Qt.createComponent("ConfirmLan.qml");
            if (component.status == Component.Ready) {
                var dialog = component.createObject(root)
                dialog.value = languageValue
            }
        }
    }
}
