import QtQuick 2.1
import QtQuick.Controls 1.1
Rectangle {
    id: root
    width: parent.width
    height: parent.height
    property string sameMsg: "<br>项目经验这块我就不在这里多写了。<br>毕竟在技能和项目两个模块里面写的已经够多的了。<br><br>总结起来就是跨平台的应用。<br>对应的框架就是Qt与HTML5，语言就是JS，C++部分能力仅仅在于能看懂或者修改一些功能代码...<br>JavaScript用了2年，开发毕竟比较熟练...<br>最近一个app只用了不到2周时间。就算这个简历，东拼西凑的时间也就1周左右....<br><br>.........................................................................<br>熟练使用的2个框架sencha touch 和 Qt Quick <br>其中到我写下这写文字的时候，Qt Quick使用才一个月，参与和独立完成的APP一共4个（2参与 2独立）,多余的不废话，希望有机会参与贵公司的更为优秀的产品..."
    Rectangle {
        id: bg
        x: 5
        y: 5
        width: parent.width - 10
        height: parent.height - 10
        anchors.centerIn: parent
        color: parent.color
        border.color: "#ccc"
        border.width: 1
        radius: 5
    }
    Rectangle {
        id: topMsg
        width: bg.width - 20
        height: msg.height + 10
        anchors.top: bg.top
        anchors.topMargin: 5
        anchors.horizontalCenter: bg.horizontalCenter
        Text {
            id: msg
            anchors.centerIn: parent
            x: 5
            clip: true //是否允许被修剪
            color: "red" //颜色
            style: Text.Raised
            font.pixelSize: 17
            styleColor: "red"
            width: parent.width - 10
            wrapMode: Text.WrapAnywhere
        }
        Timer {
                id: time
                interval: 100
                repeat: true
                running:true
                onTriggered: {
                var value = sameMsg.substring(0, 1);
                msg.text +=  value ;
                 sameMsg = sameMsg.substring(1,sameMsg.length);
                  if(sameMsg.length == 0){
                        time.running = false;
                  }
                }
            }
    }
}
