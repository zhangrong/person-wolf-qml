import QtQuick 2.0
import "qrc:/tool/component" as Tools
Item {
    width:parent.width
    height:parent.height
    Component.onCompleted: showPageByTap()
    Tools.ToolBar{
        width:parent.width
        height:90
        id:titlebar
        title: load.pageTitle
        backButtonShow: true
        onBack: {
            load.setActiveItemByName("center");
        }
    }
    Item{
        id:child
        anchors.top:titlebar.bottom
        width: parent.width
        height: parent.height - titlebar.height
    }
    function showPageByTap(){
        var items = "" ;
        switch (load.lastPage){
        case "skill":
            items =  "../message/BaseSkill.qml"
          break;
        case "projects":
             items =  "../message/BaseProjects.qml"
          break;
        case "experience":
             items =  "Experience.qml"
          break;
        case "assessment":
           items = "../message/BaseAssessment.qml";
          break;
        case "english":
           items = "Language.qml";
          break;
        }
         Qt.createComponent(items).createObject(child);
    }
}
