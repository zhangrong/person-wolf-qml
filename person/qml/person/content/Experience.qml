import QtQuick 2.0
import "qrc:/tool/component" as Tools
//import "ExpeData.qml" as Data
Item {
    id: root
    width: parent.width
    height: parent.height
    Rectangle {
        id: bg
        x: 5
        y: 5
        width: parent.width - 10
        height: parent.height - 10
        anchors.centerIn: parent
        color: "#ccc"
        opacity: .5
        border.color: "#ccc"
        border.width: 1
        radius: 5
    }
    Flickable  {
        id: topMsg
        width: bg.width - 20
        height: bg.height - 10
        x: 5
        anchors.top: bg.top
        anchors.topMargin: 5
        anchors.horizontalCenter: bg.horizontalCenter
        contentWidth: bg.width - 20;
        contentHeight:firProject.height+secProject.height + 30
        clip:true
        Column {
            id:msg
            anchors.fill: parent
            spacing: 25
            Item{
                width:parent.width
                height:firProject.height
                Column{
                    id:firProject
                    width:parent.width
                    spacing: 20
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        //text:ExpeData.zhCompany
                       // text:"<font color='red'>公                        司：</font>&nbsp;北京哈哈莉莉信息有限公司"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>职                       位：</font>&nbsp;java程序员"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>在岗时间：</font>&nbsp;2012年"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>负责项目：</font>&nbsp;1>爬虫项目;        2>淘教中国手机客户端;"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>项目简介：</font>爬虫项目：  爬虫项目是基于j2ee的一个抓取指定网站的指定信息的项目中的一个功能,以截取网页源码中的信息内容,并保存到数据库,之后再通过老大的框架实现对数据的增删改查... (用时3个月完成)<br> <br>  淘教中国手机客户端：客户端是使用jquery.mobile+phonegap进行开发,完全属于独立开发,在无对此熟悉的人的帮忙,自己独立研究的情况下,完成基本界面,百度地图的调用与位置查询,百度定位,轮询推送..等一系列功能..."
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>项目总结：</font>以上这些经验是本人第一次尝试写代码,为后来的程序开发做了很好的基础,从自学java（使用3个月）到自学js到css,jquery的使用,以及跨平台的框架的开发奠定了基础。虽然自学的过程比较缓慢,但是后期随着经验的累积,使本人在同类框架从接触到开发应用使用的时间都很短...."
                    }
                }
            }
            Item{
                width:parent.width
                height:secProject.height
                Column{
                    id:secProject
                    width:parent.width
                    spacing: 20
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>公                        司：</font>&nbsp;苏州晟点信息科技有限公司"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>职                       位：</font>&nbsp;前端手机app开发"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>在岗时间：</font>&nbsp;2013到当前"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>负责项目：</font>&nbsp;1>参与和独立开发公司的8款手机app"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>项目简介：</font>8款app包括电子菜谱网络版和离线版,点菜机的网络版和离线版,客户端信息管理器,微信点菜器,Qt版单机版电子菜谱和点菜机的开发和维护"
                    }
                    Text{
                        style: Text.Raised
                        font.pixelSize: 17
                        width:parent.width
                        wrapMode: Text.WrapAnywhere
                        text:"<font color='red'>项目总结：</font>使用了近2年的sencha touch做6个正式的项目以及不计数的几个小东西,从命令创建项目,到完成点菜以及提交订单，从使用框架功能到使用插件,到改写插件,到性能维护。都有很大的成长"
                    }
                }
            }
        }
    }
}
