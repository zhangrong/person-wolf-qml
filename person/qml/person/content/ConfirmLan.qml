import QtQuick 2.0
import "qrc:/tool/component" as Tools
Item {
    id: root
    width: parent.width
    height: parent.height
    property var  value
    Tools.ConfirmDialog{
        text: qsTr("lanChange")
        leftButtonText: qsTr("cancel")
        rightButtonText: qsTr("confirm")
        onSubmit:{
            load.language.language = value;
            Qt.quit();
        }
    }
}
