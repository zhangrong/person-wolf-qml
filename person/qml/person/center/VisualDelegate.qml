import QtQuick 2.0
import QtQml.Models 2.1
import "qrc:/tool/component" as Tools
import "../message"
Item{
    id:del
    width: grid.cellWidth
    height:grid.cellHeight
    Tools.ColorArea{
        id:wrapper
        topColor: mouse.pressed?"#ccc":"white"
        bottomColor:  mouse.pressed? "#ddd":"#ccc"
    }
    Text {
        text: display
        color:"#423b3b"
        anchors.centerIn: parent
        font.bold: true
        font.pointSize: 15
    }
    MouseArea{
        id:mouse
        anchors.fill: parent
        onClicked:{
                load.lastPage = logogram;
                load.pageTitle = display;
                load.setActiveItemByName(affiliation);
            }
    }
}
