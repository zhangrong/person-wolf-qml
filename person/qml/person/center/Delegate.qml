import QtQuick 2.0
Rectangle {
    id:carView
    width: view.width
    height: view.height
    Image {
        id:img
        width: parent.width
        height: parent.height
        source: image
        Rectangle{
            id:baseMsg
            color: "#eae7e7"
            opacity:0.3
            width:parent.width
            height:50
            anchors.bottom:parent.bottom
        }
        Text{
            id:title
            anchors.top:baseMsg.top
            anchors.bottom: baseMsg.bottom
            anchors.margins: 5
            font.pointSize: 12
            x: 10
            color: "#383737"
            text:"<font color='red' size='17'>"+qsTr("intro")+"：</font>"+content
            width:parent.width-20
            wrapMode: TextEdit.Wrap//自动换行
            elide: Text.ElideRight//超过多余的地方用...代替
        }
    }
}
