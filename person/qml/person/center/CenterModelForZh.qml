import QtQuick 2.0
import QtQml.Models 2.1
ListModel {
    ListElement { display: "基本信息";affiliation:"message";logogram:"message" }
    ListElement { display: "专业技能" ;affiliation:"content";logogram:"skill"}
    ListElement { display: "工作经验";affiliation:"content";logogram:"experience" }
    ListElement { display: "所做项目" ;affiliation:"content";logogram:"projects"}
    ListElement { display: "工作总结" ;affiliation:"content";logogram:"assessment"}
    ListElement { display: "英文简历";affiliation:"content";logogram:"english" }
}
