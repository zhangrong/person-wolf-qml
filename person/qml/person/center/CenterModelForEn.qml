import QtQuick 2.0
import QtQml.Models 2.1
ListModel {
    ListElement { display: "BaseInfo";affiliation:"message";logogram:"message" }
    ListElement { display: "Skills" ;affiliation:"content";logogram:"skill"}
    ListElement { display: "Experience";affiliation:"content";logogram:"experience" }
    ListElement { display: "Projects" ;affiliation:"content";logogram:"projects"}
    ListElement { display: "Summary" ;affiliation:"content";logogram:"assessment"}
    ListElement { display: "English";affiliation:"content";logogram:"english" }
}
