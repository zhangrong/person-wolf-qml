import QtQuick 2.0
import QtQml.Models 2.1
import "qrc:/tool/component" as Tools
Item {
    id: root
    width: parent.width
    height: parent.height
    property int timeRun
    //界面加载完成后
    Tools.Timer{
        id:time
        running: timeRun == 1
        interval:500
        repeat:false
        onTriggered:{
            view.currentIndex = 0
        }
    }
    Component.onCompleted: {
        if(currentLanguage == "zh-CN"){
            grid.model =   zhCenterModel

        }
      else{
            grid.model =   enCenterModel
        }
        view.currentIndex = 0;
    }
    CenterModelForZh{
      id:zhCenterModel
    }
    CenterModelForEn{
      id:enCenterModel
    }
    ProjectModelForEn{
        id:enProjectModel
    }
    ProjectModelForZh{
        id:zhProjectModel
    }
    GridView {
        id:grid
        anchors.top:indexBar.bottom
        height:parent.height*0.4
        width: parent.width
        cellWidth: parent.width/2
        cellHeight: 50
        delegate:VisualDelegate{}
    }
    Rectangle{
        id:topMsg
        color: "#f5ecec"
        opacity: 0.5
        anchors.top: parent.top
        width: parent.width
        height: 50
        z:view.z + 1
    }
    Text{
        anchors.centerIn: topMsg
        z:topMsg.z+1
        font.bold: true
        font.pixelSize: 20
        color: "#342223"
        text:"我会的跨平台工具"
    }
    ListView {
        id: view
        anchors.top: parent.top
        height: parent.height-170
        width:parent.width
        model:SkillModel{}
        delegate:Delegate{}
        highlightRangeMode: ListView.StrictlyEnforceRange
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem;
    }
    Rectangle {
        id:indexBar
        width: root.width; height:20
        color: "#f5ecec"
        opacity: 0.4
        anchors { top: view.bottom}
        Row {
            anchors.centerIn: parent
            spacing: 20
            Repeater {
                model: view.model.count
                Rectangle {
                    width: 5; height: 5
                    radius: 3
                    color: view.currentIndex == index ? "#a00909" : "white"
                    MouseArea {
                        width: 20; height: 20
                        anchors.centerIn: parent
                        onClicked: view.currentIndex = index
                    }
                }
            }
        }
    }
}
