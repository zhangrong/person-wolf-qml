import QtQuick 2.0
import QtQuick.Controls 1.1
Item {
    id: mainView
    width: parent.width
    height: parent.height
    property real barOpacity : 0.4
    Image {
        source:"../images/background.png"//背景图片
        anchors.fill: parent
        ListView {
            id: list
            anchors.top: topBar.bottom
            anchors.bottom: footBar.top
            width: parent.width
            model: MainModel{}
            delegate:mainDelegate
        }
        Component {
            id: mainDelegate
            Item {
                id: recipe
                //定义一个属性，目的是为了在详细页淡出的时候透明化详细页
                property real detailsOpacity : 0
                width: parent.width
                height: 90
                opacity:0.5
                Text {
                    id:nameArea
                    text: name
                    font.bold: true
                    font.pointSize: 16
                    anchors.centerIn: parent
                    color: "White"
                }
                Rectangle {
                    id: rect
                    x: 20
                    border.color: "White"
                    border.width: 2
                    height: 70
                    width: 70
                    radius: 12
                    antialiasing: true
                    anchors.verticalCenter: parent.verticalCenter
                    Image {
                        source:"../images/wolf_head.png"//背景图片
                        anchors.fill: parent
                    }
                    states : State {
                        name: "right"
                        PropertyChanges {
                            target: rect
                            x: parent.width - 90
                            color: "#cccccc"
                        }
                    }
                    transitions: Transition {
                        NumberAnimation { properties: "x"; easing.type:  Easing.OutQuad; duration: 1000 }
                    }
                }
                MouseArea {
                    onClicked: {
                        if (rect.state == ''){
                            rect.state = "right";
                            recipe.state = 'Details';
                            barOpacity = 0
                        }
                    }
                    anchors.fill: parent
                    anchors.margins: -5
                }
                Rectangle{
                    id:backImg
                    anchors.top: parent.top
                    height:50
                    width: parent.width
                    opacity: recipe.detailsOpacity
                    Image {
                        anchors { left:parent.left; leftMargin: 10 }
                        source: "../images/back.png"
                        height: parent.height
                        width:50
                        MouseArea{
                            id:imgMouse
                            anchors.fill: parent
                            onClicked:{
                                barOpacity = 0.4;
                                recipe.state = '';
                            }
                        }
                    }
                    Rectangle{
                        anchors.centerIn: backImg
                        height:backImg.height - 15
                        width: backImg.width - 15
                        opacity: 0.2
                        radius: 5
                        color:"#cccccc"
                        visible: imgMouse.pressed
                    }
                    Text{
                        anchors.centerIn: parent
                        text:name
                        color: "red"
                        opacity: 0.5
                        font.bold: true
                        font.pixelSize: 20
                    }
                }

                states: State {
                    name: "Details"
                    //属性改变
                    PropertyChanges { target: topBar; visible:false}
                    PropertyChanges { target: footBar; visible:false}
                    PropertyChanges { target: rect; visible:false }
                    PropertyChanges { target: nameArea; visible:false }
                    PropertyChanges { target: recipe; detailsOpacity: 1; x: 0 } // 详细显示
                    PropertyChanges { target: recipe; height: mainView.height } // 高等于屏幕的高
                    // 移动顶部的列表，使y轴的值扩大到recipe的y
                    PropertyChanges { target: recipe.ListView.view; explicit: true; contentY: recipe.y +90}
                    // 不允许移动当前视图
                    PropertyChanges { target: recipe.ListView.view; interactive: false }
                }
                transitions: Transition {
                    // 确保状态变化平稳
                    ParallelAnimation {
                        ColorAnimation { property: "color"; duration: 500 }//颜色变化在0.5秒内完成
                        NumberAnimation { duration: 300; properties: "detailsOpacity,x,contentY,height,width" }
                    }
                }
            }
        }
        Rectangle {
            id: topBar
            width: parent.width
            height: 90
            opacity: barOpacity
            Rectangle {
                height: 1
                color: "white"
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
            }
            Rectangle {
                height: 1
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: 1
                anchors.left: parent.left
                anchors.right: parent.right
            }
            gradient: Gradient {
                GradientStop { position: 0 ; color: "white" }
                GradientStop { position: 1 ; color: "#ccc" }
            }
            Text{
                id:topBarText
                anchors.centerIn: parent
                text:"首页"
                font.bold: true
                font.pointSize: 20
                color: "red"
            }
        }
        Rectangle {
            id: footBar
            width: parent.width
            anchors.bottom: parent.bottom
            height: 60
            opacity: barOpacity
            Rectangle {
                height: 1
                color: "white"
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
            }
            Rectangle {
                height: 1
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: 1
                anchors.left: parent.left
                anchors.right: parent.right
            }
            gradient: Gradient {
                GradientStop { position: 0 ; color: "white" }
                GradientStop { position: 1 ; color: "#ccc" }
            }
            Text{
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                text:qsTr("promise")
                font.bold: true
                font.pointSize: 17
                color: "red"
            }
        }
    }
}
