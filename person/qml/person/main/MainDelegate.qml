import QtQuick 2.1
    Item{
        id: recipe
        height: 90
        property real detailsOpacity : 0
        width: parent.width
        opacity:0.5
        Text {
            text: name
            font.bold: true
            font.pointSize: 16
            anchors.centerIn: parent
            color: "White"
        }
        Rectangle {
            id: rect
            x: 20
            border.color: "White"
            border.width: 2
            height: 70
            width: 70
            radius: 12
            antialiasing: true
            anchors.verticalCenter: parent.verticalCenter
            Image {
                source:"../images/wolf_head.png"//背景图片
                anchors.fill: parent
            }
            states : State {
                name: "right"
                PropertyChanges {
                    target: rect
                    x: parent.width - 90
                    color: "#cccccc"
                }
            }
            transitions: Transition {
                NumberAnimation { properties: "x"; easing.type:  Easing.OutQuad; duration: 1000 }
            }
        }
        MouseArea {
            onClicked: {
                if (rect.state == ''){
                    rect.state = "right";
                    time.running = true;
                }
            }
            anchors.fill: parent
            anchors.margins: -5
        }
        Item {
            id: details
            x: 10
            width: parent.width - 20
            anchors { top: rect.bottom; topMargin: 10; bottom: parent.bottom; bottomMargin: 10 }
            opacity: recipe.detailsOpacity//透明度依旧继承自recipe
            Text {
                id: methodTitle
                anchors.top: parent.top
                text: "Method"
                font.pointSize: 12; font.bold: true
            }
            Flickable {
                id: flick
                width: parent.width
                anchors { top: methodTitle.bottom; bottom: parent.bottom }
                contentHeight: methodText.height
                clip: true

                Text { id: methodText; text: name; wrapMode: Text.WordWrap; width: details.width }
            }
        }
        Text {
            y: 10
            anchors { right: parent.right; rightMargin: 10 }
            opacity: recipe.detailsOpacity
            text: "Close"
            MouseArea{
                onClicked: recipe.state = '';
            }
        }
        states: State {
            name: "Details"
            //属性改变
            PropertyChanges { target: recipe; detailsOpacity: 1; x: 0 } // 详细显示
            PropertyChanges { target: recipe; height:360 } // 高等于屏幕的高

            // 移动顶部的列表，使y轴的值扩大到recipe的y
            PropertyChanges { target: recipe.ListView.view; explicit: true; contentY: recipe.y }

            // 不允许移动当前视图
            PropertyChanges { target: recipe.ListView.view; interactive: false }
        }

        transitions: Transition {
            // 确保状态变化平稳
            ParallelAnimation {
                ColorAnimation { property: "color"; duration: 500 }//颜色变化在0.5秒内完成
                NumberAnimation { duration: 300; properties: "detailsOpacity,x,contentY,height,width" }
            }
        }
    Timer {//计时器
        id:time
        interval: 1000//间隔
        running: false//运行状态
        repeat: false//不重复
        onTriggered: {
            recipe.state = 'Details';
            time.running = false;//关闭运行
        }
     }
}
