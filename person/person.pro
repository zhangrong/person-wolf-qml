# Add more folders to ship with the application, here
folder_01.source = qml/person
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01
QT += qml quick widgets gui sql
# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp\
                        ../../tool-qml-helem/plugins/language/language.cpp
HEADERS += ../../tool-qml-helem/plugins/language/language.h
RESOURCES += \
    ../../tool-qml-helem/tool.qrc\
     i18n.qrc
# Installation path
# target.path =
TRANSLATIONS += translations/zh-CN.ts \
                translations/en-US.ts
# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()
OTHER_FILES += \
    qml/person/center/VisualDelegate.qml \
    android/AndroidManifest.xml \
    qml/person/center/ProjectModelForZh.qml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
