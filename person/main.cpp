#include <QApplication>
#include <QtQml>
#include "../../tool-qml-helem/plugins/language/language.h"
#include "qtquick2applicationviewer.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QtQuick2ApplicationViewer viewer;
    QString qmFilePath;
    QString language;
    QFile file("language.ini");
    qmlRegisterType<Language>("Language",1,0,"Language");
    QSettings *settings = new QSettings("language.ini", QSettings::IniFormat);
    settings->setIniCodec(QTextCodec::codecForName("UTF-8"));
    if(file.exists()){
        language = settings->value("language").toString();
    }else{
        settings->setValue("language","en-US");
        language = "en-US";
    }
    if(language == "en-US"){
        qmFilePath = ":/translations/en-US.qm";
    }else{
        qmFilePath = ":/translations/zh-CN.qm";
    }
    QTranslator *translator = new QTranslator(qApp);
    translator->load(qmFilePath);
    qApp->installTranslator(translator);
    viewer.rootContext()->setContextProperty("currentLanguage",language);
    viewer.setMainQmlFile(QStringLiteral("qml/person/person.qml"));
    viewer.showExpanded();
    int returnCode = app.exec();
    return returnCode;
}
