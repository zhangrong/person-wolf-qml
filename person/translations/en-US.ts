<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>BaseSkill</name>
    <message>
        <location filename="../qml/person/message/BaseSkill.qml" line="37"/>
        <source>skill</source>
        <translation type="unfinished">Skill</translation>
    </message>
</context>
<context>
    <name>CenterModel</name>
    <message>
        <source>baseMsg</source>
        <translation type="obsolete">Base Message</translation>
    </message>
    <message>
        <source>skill</source>
        <translation type="obsolete">Skill</translation>
    </message>
    <message>
        <source>experience</source>
        <translation type="obsolete">Experience</translation>
    </message>
    <message>
        <source>projects</source>
        <translation type="obsolete">Projects</translation>
    </message>
    <message>
        <source>summa</source>
        <translation type="obsolete">Summa</translation>
    </message>
    <message>
        <source>english</source>
        <translation type="obsolete">English</translation>
    </message>
</context>
<context>
    <name>ConfirmLan</name>
    <message>
        <location filename="../qml/person/content/ConfirmLan.qml" line="9"/>
        <source>lanChange</source>
        <translation type="unfinished">Language change</translation>
    </message>
    <message>
        <location filename="../qml/person/content/ConfirmLan.qml" line="10"/>
        <source>cancel</source>
        <translation type="unfinished">Cancel</translation>
    </message>
    <message>
        <location filename="../qml/person/content/ConfirmLan.qml" line="11"/>
        <source>confirm</source>
        <translation type="unfinished">Confirm</translation>
    </message>
</context>
<context>
    <name>Delegate</name>
    <message>
        <location filename="../qml/person/center/Delegate.qml" line="27"/>
        <source>intro</source>
        <translation type="unfinished">Introduction</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/person/main/Main.qml" line="196"/>
        <source>promise</source>
        <translation type="unfinished">Even if bound never yield!</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../qml/person/message/Message.qml" line="48"/>
        <source>promise</source>
        <translation type="unfinished">Even if bound never yield!</translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../qml/person/loading/Splash.qml" line="18"/>
        <source>hello</source>
        <translation type="unfinished">Hi &lt;br/&gt; I&apos;m ZhuiMengLiuYun</translation>
    </message>
    <message>
        <location filename="../qml/person/loading/Splash.qml" line="39"/>
        <source>user</source>
        <translation type="unfinished">Besign:Helem &lt;br/&gt; Code:Zhang Rong</translation>
    </message>
</context>
</TS>
