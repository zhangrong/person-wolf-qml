<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>BaseSkill</name>
    <message>
        <location filename="../qml/person/message/BaseSkill.qml" line="37"/>
        <source>skill</source>
        <translation type="unfinished">工作技能</translation>
    </message>
</context>
<context>
    <name>CenterModel</name>
    <message>
        <source>baseMsg</source>
        <translation type="obsolete">技能简介</translation>
    </message>
    <message>
        <source>skill</source>
        <translation type="obsolete">工作技能</translation>
    </message>
    <message>
        <source>experience</source>
        <translation type="obsolete">工作经验</translation>
    </message>
    <message>
        <source>projects</source>
        <translation type="obsolete">所做项目</translation>
    </message>
    <message>
        <source>summa</source>
        <translation type="obsolete">信息总结</translation>
    </message>
    <message>
        <source>english</source>
        <translation type="obsolete">英文简历</translation>
    </message>
</context>
<context>
    <name>ConfirmLan</name>
    <message>
        <location filename="../qml/person/content/ConfirmLan.qml" line="9"/>
        <source>lanChange</source>
        <translation type="unfinished">当前语言已更换，重启加载？</translation>
    </message>
    <message>
        <location filename="../qml/person/content/ConfirmLan.qml" line="10"/>
        <source>cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../qml/person/content/ConfirmLan.qml" line="11"/>
        <source>confirm</source>
        <translation type="unfinished">确认</translation>
    </message>
</context>
<context>
    <name>Delegate</name>
    <message>
        <location filename="../qml/person/center/Delegate.qml" line="27"/>
        <source>intro</source>
        <translation type="unfinished">简介</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/person/main/Main.qml" line="196"/>
        <source>promise</source>
        <translation type="unfinished">被束缚又如何,绝不屈服！</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../qml/person/message/Message.qml" line="48"/>
        <source>promise</source>
        <translation type="unfinished">被束缚又如何,绝不屈服！</translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../qml/person/loading/Splash.qml" line="18"/>
        <source>hello</source>
        <translation type="unfinished">你好,亲！&lt;br/&gt; 我是追梦流云!</translation>
    </message>
    <message>
        <location filename="../qml/person/loading/Splash.qml" line="39"/>
        <source>user</source>
        <translation type="unfinished">设计：Helem &lt;br/&gt; 编码：张荣</translation>
    </message>
</context>
</TS>
